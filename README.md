# zsh-themes

My own zsh themes.

## Themes

    $dir = current working directory (white)
    $branch = current git branch (yellow)
    $venv = active python venv (purple)

Name  | Left prompt                               | Right prompt
------|-------------------------------------------|---------------
gamma | `[dir:$dir branch:$branch venv:$venv] Γ ` | none
delta | `[$dir $branch $venv] Δ `                 | none
xi    | `[$dir] Ξ `                               | `[$venv $branch]`

Other text like the brackets or the greek symbols (Γ, Δ, Ξ) is blue. If one is
not inside a git repo or there is no active venv, irrelevant information (like
in the gamma-theme `"venv:"`) will not be displayed.
