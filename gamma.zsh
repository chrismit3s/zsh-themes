# TODO
#  [_] remove helper functions and color rename from themes to seperate file

# better names for colors
for color in red green yellow blue magenta cyan black white; do
	eval "${color}      () { echo      \"%{$fg[${color}]%}\$1%{$reset_color%}\" }"
	eval "${color}_bold () { echo \"%{$fg_bold[${color}]%}\$1%{$reset_color%}\" }"
done

# helpers
git_branch () {
	echo "${$(git branch --quiet 2>/dev/null):2}"
}
python_venv () {
	echo "${VIRTUAL_ENV##*/}"
}

# prompts
left_prompt () {
	branch="$(git_branch)"
	venv="$(python_venv)"

	ret="$(blue '[')dir:%~"
	[[ "$branch" != "" ]] && ret="$ret$(yellow  ' branch:'$branch)"
	[[ "$venv"   != "" ]] && ret="$ret$(magenta ' venv:'$venv)"
	ret="$ret$(blue '] Γ ')"

	echo "$ret"
}
right_prompt () {
	echo ""
}
PROMPT='$(left_prompt)'
RPROMPT='$(right_prompt)'
